/**
 * Hella Backend API
 * utils/Errors.js
 * Jean Oliveira 03/11/2017
 *
 * Errors for the project
 *
 */
"use strict"
let mongoose = require('mongoose')
let Boom = require('boom')

module.exports.CONVERSION = {
	TO_FLOAT: { type: 'C001', description: 'Error converting value to float. A float number was expected by other thing was sent' }
}

module.exports.PROPERTY = {
	WRONG_MISSING: { type: 'P001', description: 'Property undefined, with a wrong name, or without proper content on a context where it needs to be existent' }
}

module.exports.OPERATIONS = {
	GET_OPERATION: { type: 'OK', status: 200, description: 'Data got ok' },
	LOGIN: { type: 'DENY', status: 401, content: Boom.unauthorized('Invalid or missing credentials. Provide valid email or CPF and password') },
	INVALID_ACTION: { type: 'BAD ARGUMENTS', status: 400, content: Boom.badRequest('You must provide a valid action name') },
	INVALID_QUERY: { type: 'BAD ARGUMENTS', status: 400, content: Boom.badRequest('You must provide a valid property name and content') },
	NO_TOKEN: { type: 'BAD ARGUMENTS', status: 400, content: Boom.badRequest('You must provide a authorization token') },
}
