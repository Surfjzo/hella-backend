/**
 * Hella Backend API
 * utils/Permissions.js
 * Jean Oliveira 03/11/2017
 *
 * Provides permissions services
 *
 */
"use strict"
let Constants = require('./Constants'),
	Enums = require('./Enums'),
	Boom = require('boom'),
	jwt = require('jsonwebtoken'),
	Error = require('./Errors')

module.exports.authorize = (roleNeeded) => {
	return async function (req, res, next) {
		try {
			if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
				let token = req.headers.authorization.split(' ')[1]
				let decoded = await jwt.verify(token, Constants.SESSION_SECRET)
				if (decoded.userTypes && decoded.userTypes instanceof Array) {
					if (hasLevelOf(decoded.userTypes, roleNeeded)) {
						req.user = decoded
						next()
					} else {
						throw Error.OPERATIONS.LOGIN
						//return res.send(403).json(Boom.forbidden('You must have the right credentials to use this resource.'))
					}
				} else {
					throw Error.OPERATIONS.NO_TOKEN
				}
			} else {
				throw Error.OPERATIONS.NO_TOKEN
			}
		} catch (blockError) {
			return res.json(blockError)
		}
	}
}

async function hasLevelOf (rolesArray, roleNeeded) {
	return new Promise(async (resolve, reject) => {
		try {
			if (roleNeeded instanceof Enums.USER_TYPE && rolesArray instanceof Array) {
				if (rolesArray.includes(roleNeeded)) {
					return resolve(true)
				}
				for (let role in rolesArray) {
					if (role.value >= roleNeeded.value) {
						return resolve(true)
					}
				}
			} else {
				return reject(false)
			}
		} catch (e) {
			return reject(false)
		}
	})
}
