/**
 * Hella Backend API
 * utils/JoiSchemas.js
 * Jean Oliveira 25/11/2017
 *
 * Joi Schemas for the project
 *
 */
"use strict"
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Joi = require('joi')
const Constants = require('./Constants')

const areaCodeSchema = Schema({
	value: { type: String, required: true },
	description: { type: String, required: true }
})

const descriptionSchema = Schema({
	en: { type: String },
	pt: { type: String }
})

const userTypeSchema = Schema({
	value: { type: Number, min: 1, max: 100, required: true },
	description: { type: descriptionSchema, required: true }
})

const estimateStatusSchema = Schema({
	value: { type: Number, min: 1, max: 8, required: true },
	description: { type: descriptionSchema, required: true }
})

const mediaTypeSchema = Schema({
	value: { type: Number, min: 1, max: 3, required: true },
	description: { type: descriptionSchema, required: true }
})

const clearanceStatusSchema = Schema({
	value: { type: Number, min: 1, max: 6, required: true },
	description: { type: descriptionSchema, required: true }
})

const bankAccountSchema = Schema({
	bankCode: { type: String, required: true },
	bankName: { type: String, required: true },
	bankBranch: { type: String, required: true },
	accountNumber: { type: String, required: true }
})

const phoneSchema = Schema({
	number: { type: String, required: true },
	phoneType: { type: String, required: true }
})

module.exports.DB_SCHEMAS = {
	AREA_CODE_SCHEMA: areaCodeSchema,
	DESCRIPTION_SCHEMA: descriptionSchema,
	USER_TYPE_SCHEMA: userTypeSchema,
	ESTIMATE_STATUS_SCHEMA: estimateStatusSchema,
	MEDIA_TYPE_SCHEMA: mediaTypeSchema,
	CLEARANCE_STATUS_SCHEMA: clearanceStatusSchema,
	BANK_ACCOUNT_SCHEMA: bankAccountSchema,
	PHONE_SCHEMA: phoneSchema
}

const addressesSchema = Joi.array().items(Joi.object({
	_id: Joi.string().optional(),
	label: Joi.string().required(),
	zipCode: Joi.string().required(),
	street: Joi.string().required(),
	number: Joi.string().required(),
	district: Joi.string().required(),
	complement: Joi.string().optional().allow(''),
	city: Joi.string().required(),
	state: Joi.string().required(),
	country: Joi.string().required()
}).required()).min(1).unique().required()

const phonesSchema = Joi.array().items(Joi.object({
  _id: Joi.string().optional(),
	number: Joi.string().min(7).required(),
	phoneType: Joi.string().min(1).required()
}).required()).min(1).unique().required()

const newUserSchema = Joi.object().keys({
	name: Joi.string().min(5).required(),
	cpf: Joi.string().regex(Constants.REGEX_CPF).required(),
	email: Joi.string().email().required(),
	addresses: addressesSchema,
	password: Joi.string().min(6).required(),
	phones: phonesSchema
}).required()

const userLoginSchema = Joi.object().keys({
	cpf: Joi.string().regex(Constants.REGEX_CPF),
	email: Joi.string().regex(Constants.REGEX_EMAIL),
	password: Joi.string().required()
}).required()

const updateUserSchema = Joi.object().keys({
  _id: Joi.string().required(),
	__v: Joi.number().required(),
	name: Joi.string().min(5),
	email: Joi.string().email().required(),
	addresses: addressesSchema,
	password: Joi.string().min(6),
	phones: phonesSchema,
	cpf: Joi.string().required(),
	able: Joi.boolean().required(),
	creation: Joi.string().required(),
	socialEvalPro: Joi.object().required(),
	socialEvalClient: Joi.object().required(),
	cnaes: Joi.array().required(),
	documents: Joi.array().required(),
	areaCodes: Joi.array().required(),
	types: Joi.array().required(),
	creditCards: Joi.array().required(),
	cnpjs: Joi.array().required()
})

module.exports.ROUTE_SCHEMAS = {
	NEW_USER_SCHEMA: newUserSchema,
	USER_LOGIN_SCHEMA: userLoginSchema,
	UPDATE_USER_SCHEMA: updateUserSchema
}