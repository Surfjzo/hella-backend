/**
 * Hella Backend API
 * utils/Hashing.js
 * Jean Oliveira 03/11/2017
 *
 * Provides hashing services
 *
 */
"use strict"
let bcrypt = require('bcrypt-nodejs')
let Constants = require('./Constants')

module.exports.passwordHash = function (plainTextPassword) {
	return new Promise((resolve, reject) => {
		let saltFactor = parseInt(Constants.SALT_WORK_FACTOR)
		bcrypt.genSalt(saltFactor, function (saltError, salt) {
			if (saltError) {
				console.error('utils/hashing error generating salt', saltError)
				return reject({error: { type: 'Error generation salt for password hashing', details: saltError }})
			}
			// hash the password along with our new salt
			bcrypt.hash(plainTextPassword, salt, null, function (hashError, hashedPassword) {
				if (hashError) {
					console.error('utils/hashing error hashing password', hashError)
					return reject({error: {type: 'Error generation hashed password', details: hashError}})
				}
				// override the cleartext password with the hashed one
				//console.log('Hashed', hashedPassword)
				return resolve(hashedPassword)
			})
		})
	})
}

module.exports.verifyPassword = function (plainTextPass, hashedPassword) {
	return new Promise((resolve, reject) => {
		bcrypt.compare(plainTextPass, hashedPassword, function (compareError, result) {
			if (compareError){
				console.error('utils/hashing error comparing password', compareError)
				return reject({error: {type: 'Error comparing password and hash', details: compareError}})
			}else {
				return resolve(result)
			}
		})
	})
}
