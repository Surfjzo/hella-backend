/**
 * Hella Backend API
 * utils/Logger.js
 * Jean Oliveira 03/11/2017
 *
 * Logger for the project
 *
 */
"use strict"
let winston = require('winston')
winston.level = process.env.MODE === 'development' ? 'debug' : 'verbose'

const tsFormat = () => (new Date()).toISOString()
//const tsFormat = () => (new Date()).toLocaleString()

const logger = new (winston.Logger)({
	transports: [
		new (winston.transports.Console)({
			timestamp: tsFormat,
			colorize: true,
		})
	]
})

module.exports.info = (TAG, message) => {
	logger.log('info', '['+TAG+'] '+message)
}

module.exports.warn = (TAG, message) => {
	logger.log('warn', '['+TAG+'] '+message)
}

module.exports.error = (TAG, message) => {
	logger.log('error', '['+TAG+'] '+message)
}

module.exports.debug = (TAG, message) => {
	logger.log('debug', '['+TAG+'] '+message)
}