/**
 * Hella Backend API
 * utils/Successes.js
 * Jean Oliveira 03/11/2017
 *
 * Successes for the project
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema

module.exports.OPERATIONS = {
	GET_OPERATION: { type: 'OK', status: 200, description: 'Data got ok' },
	LOGIN: { type: 'OK', status: 200, description: 'Login successful'}
}