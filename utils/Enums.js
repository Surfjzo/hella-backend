/**
 * Hella Backend API
 * utils/Enums.js
 * Jean Oliveira 03/11/2017
 *
 * Enumerations for the project
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema

module.exports.COLLECTION_NAMES = {
	USER: 'users',
	COMPANY: 'companies'
}

module.exports.USER_TYPE = {
	CUSTOMER: { value: 1, description: { en: 'Customer', pt: 'Cliente' } },
	SUPPLIER: { value: 2, description: { en: 'Supplier', pt: 'Fornecedor' } },
	SERVICE_PROVIDER: { value: 3, description: { en: 'Service Provider', pt: 'Prestador de Serviço' } },
	SKILLED_LABOUR: { value: 4, description: { en: 'Skilled Labour', pt: 'Mão de Obra Qualificada' } },
	LICENSEE: { value: 10, description: { en: 'Licensee', pt: 'Licenciado' } },
	MANAGER: { value: 20, description: { en: 'Manager', pt: 'Gerente' } },
	ADMIN: { value: 100, description: { en: 'Admin', pt: 'Admin' } },
}

module.exports.ESTIMATE_STATUS = {
	OPEN: { value: 1, description: { en: 'Open', pt: 'Em aberto' } },
	APPROVED: { value: 2, description: { en: 'Approved', pt: 'Aprovado' } },
	IN_PROGRESS: { value: 3, description: { en: 'In Progress', pt: 'Em Andamento' } },
	ON_HOLD: { value: 4, description: { en: 'On Hold', pt: 'Suspenso' } },
	CANCELED: { value: 5, description: { en: 'Canceled', pt: 'Cancelado' } },
	SERVICE_DONE: { value: 6, description: { en: 'Service Done', pt: 'Concluído' } },
	AWAITING_PAYMENT: { value: 7, description: { en: 'Awaiting Payment', pt: 'Aguardando Pagamento' } },
	PAID: { value: 8, description: { en: 'Paid', pt: 'Pago' } },
}

module.exports.MEDIA_TYPE = {
	RECORDS: { value: 1, description: { en: 'Records', pt: 'Documento' } },
	PHOTO: { value: 2, description: { en: 'Photo', pt: 'Foto' } },
	VIDEO: { value: 3, description: { en: 'Video', pt: 'Vídeo' } },
}

module.exports.CLEARANCE_STATUS = {
	PAID: { value: 1, description: { en: 'Paid', pt: 'Pago' } },
	WRONG_DATA: { value: 2, description: { en: 'Wrong Account Information', pt: 'Informações de Conta Incorretas' } },
	DUE: { value: 3, description: { en: 'Due', pt: 'Devido' } },
	NOT_DUE_YET: { value: 4, description: { en: 'Not Due Yet', pt: 'Ainda Não Devido' } },
	BANK_ERROR: { value: 5, description: { en: 'Bank Error', pt: 'Erro do Banco' } },
	RETURNED: { value: 6, description: { en: 'Returned By Receiver', pt: 'Devolvido Pelo Sacador' } },
}

module.exports.PAYMENT_TYPE = {
	CREDIT_CARD: 1,
	CASH: 2,
	WIRE_TRANSFER: 3,
	PAYMENT_ORDER: 4,
}

