let express = require('express')
let path = require('path')
let favicon = require('serve-favicon')
let logger = require('morgan')
let cookieParser = require('cookie-parser')
let bodyParser = require('body-parser')
let Permissions = require('./utils/Permissions')
let Enum = require('./utils/Enums')
let Constants = require('./utils/Constants')

let index = require('./routes/index')
//let users = require('./routes/users')

let app = express()

// ** ROUTES REQUIREMENTS BEGIN **
// System routes
let adminRoutes = require('./routes/system/adminRoutes')

// User routes
let userRoutes = require('./routes/user/userRoutes')
let signUserRoutes = require('./routes/user/signUserRoutes')

// ** ROUTES REQUIREMENTS END **


// view engine setup
//app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cookieParser())
//app.use(express.static(path.join(__dirname, 'public')))
app.use((req, res, next) => {
	'use strict'
	res.setHeader("Access-Control-Allow-Origin", "*")
	res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
	next()
})

app.use('/', index)
//app.use('/users', users)

// ** ROUTES SETUP BEGIN **
// System routes
app.use('/admin', Permissions.authorize(Enum.USER_TYPE.ADMIN))
app.use('/admin/:name', adminRoutes)

// Adm Routes
app.use('/users', Permissions.authorize(Enum.USER_TYPE.LICENSEE))

// User routes
//app.use('/user', Permissions.authorize(Enums.USER_TYPE.CUSTOMER))
app.use('/user', Permissions.authorize(Enum.USER_TYPE.CUSTOMER), userRoutes)
app.use('/sign', signUserRoutes)


// ** ROUTES SETUP END **

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
