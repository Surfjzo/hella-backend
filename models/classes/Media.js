/**
 * Hella Backend API
 * models/Media.js
 * Jean Oliveira 03/11/2017
 *
 * Media model
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Schemas = require('../../utils/HellaSchemas')

const MediaSchema = Schema({
	url: { type: String, required: true },
	description: { type: String },
	analysis: { type: Date },
	approved: { type: Date },
	approvedBy: { type: Schema.Types.ObjectId },
	creation: { type: Date, required: true, default: Date.now() },
	mediaType: { type: Schemas.DB_SCHEMAS.MEDIA_TYPE_SCHEMA, required: true }
})

let Media = mongoose.model('media', MediaSchema)

module.exports.schema = MediaSchema
module.exports = Media