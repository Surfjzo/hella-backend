/**
 * Hella Backend API
 * models/Media.js
 * Jean Oliveira 03/11/2017
 *
 * Star model
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Enums = require('../../utils/Enums')
let Errors = require('../../utils/Errors')
let Successess = require('../../utils/Successes')
let Logger = require('../../utils/Logger')
const TAG = 'Star'

const StarSchema = Schema({
	interactions: { type: Number, required: true, default: 0 },
	points: { type: Number, required: true, min: 0, max: 5, default: 0 }
})

let Star = mongoose.model('star', StarSchema)

Star.prototype.getScore = () => {
	let divider = (this.interactions == 0 ? 1 : this.interactions)
	return this.points / divider
}

Star.prototype.addScore = (points = 0) => {
	try {
		let pointsToAdd = parseFloat(points)
		this.points += pointsToAdd
		this.interactions++
		return Successess.OPERATIONS
	} catch (convertToFloatError) {
		Logger.error(TAG, 'Impossible to convert points parameter to Float')
		return Errors.CONVERSION.TO_FLOAT
	}
}

module.exports.schema = StarSchema
module.exports = Star