/**
 * Hella Backend API
 * models/Cnae.js
 * Jean Oliveira 03/11/2017
 *
 * Cnae model
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema
const TAG = 'Cnae'

const CnaeSchema = Schema({
	code: { type: String, required: true },
	section: { type: String },
	sectionDescription: { type: String },
	division: { type: String },
	divisionDescription: { type: String },
	group: { type: String },
	groupDescription: { type: String },
	classs: { type: String },
	classsDescription: { type: String },
	subclass: { type: String },
	subclassDescription: { type: String },
})

let Cnae = mongoose.model('cnae', CnaeSchema)

module.exports.schema = CnaeSchema
module.exports = Cnae