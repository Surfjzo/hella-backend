/**
 * Hella Backend API
 * models/Address.js
 * Jean Oliveira 03/11/2017
 *
 * Address model
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema
const TAG = 'Address'

const AddressSchema = Schema({
	label: { type: String, required: true },
	zipCode: { type: String, required: true },
	street: { type: String, required: true },
	number: { type: String, required: true },
	district: { type: String, required: true },
	city: { type: String, required: true },
	state: { type: String, required: true },
	country: { type: String, required: true }
})

let Address = mongoose.model('address', AddressSchema)

module.exports.schema = AddressSchema
module.exports = Address