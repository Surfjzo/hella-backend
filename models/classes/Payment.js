/**
 * Hella Backend API
 * models/Payment.js
 * Jean Oliveira 03/11/2017
 *
 * Payment model
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Enums = require('../../utils/Enums')

const PaymentSchema = Schema({
	estimateId: { type: Schema.types.ObjectId, required: true },
	requesterId: { type: Schema.types.ObjectId, required: true },
	providerId: { type: Schema.types.ObjectId, required: true },
	status: { type: Enums.CLEARANCE_STATUS_SCHEMA, required: true },
	value: { type: Number, required: true },
	date: { type: Date, required: true, default: Date.now() },
	clearanceDate: { type: Date, required: true }
})

let Payment = mongoose.model('payment', PaymentSchema)

module.exports = Payment