/**
 * Hella Backend API
 * models/Media.js
 * Jean Oliveira 03/11/2017
 *
 * Phone model
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schemas = require('../../utils/HellaSchemas')

let Phone = mongoose.model('phone', Schemas.DB_SCHEMAS.PHONE_SCHEMA)

module.exports = Phone