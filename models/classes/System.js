/**
 * Hella Backend API
 * models/System.js
 * Jean Oliveira 03/11/2017
 *
 * System model
 *
 */
"use strict"
let mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	Constants = require('../../utils/Constants'),
	Enums = require('../../utils/Enums'),
	Logger = require('../../utils/Logger')

const TAG = 'SystemCore'

const SystemSchema = Schema({
	name: { type: String, required: true },
	content: { type: mongoose.Schema.Types.Mixed, required: true }
}, { collection: Constants.COLLECTION_NAME.SYSTEM });

// To save objects like AreaCode, PhoneType, Bank, etc.

let System = mongoose.model('system', SystemSchema)

module.exports.getContentForKey = async (desiredKey) => {
	Logger.info(TAG, 'Reading data for key '+ desiredKey)
	return await System.findOne({ name: desiredKey }).exec()
}

module.exports.setContentForKey = async (providedKey, providedContent) => {
	Logger.info(TAG, 'Saving content under key: '+ providedKey)
	return await System.findOneAndUpdate(
		{ name: providedKey },
		{ $set: { content: providedContent } },
		{ new: true, upsert: true } // Returns the updated object and creates a new one if doesn't exists
	).exec()
}

//module.exports = System