/**
 * Hella Backend API
 * models/CreditCard.js
 * Jean Oliveira 03/11/2017
 *
 * CredtiCard model
 *
 */
"use strict"
let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Enums = require('../../utils/Enums')

const CreditCardSchema = Schema({
	lastNumbers: { type: String, required: true },
	token: { type: String },
	date: { type: Date, required: true, default: Date.now() },
	brand: { type: String, required: true },
	cvv: { type: String, required: true },
	validity: { type: String, required: true }
})

let CreditCard = mongoose.model('creditCard', CreditCardSchema)

module.exports.schema = CreditCardSchema
module.exports = CreditCard