/**
 * Hella Backend API
 * models/User.js
 * Jean Oliveira 03/11/2017
 *
 * User model
 *
 */
"use strict"
let mongoose = require('mongoose'),
	Successes = require('../../utils/Successes'),
	Errors = require('../../utils/Errors'),
	Enums = require('../../utils/Enums'),
	User = require('../classes/User'),
	Logger = require('../../utils/Logger'),
	Hash = require('../../utils/Hashing'),
	jwt = require('jsonwebtoken'),
	Constants = require('../../utils/Constants')

const TAG = 'DbUser'

module.exports.findById = async (userId) => {
	try {
		return await User.findOne({_id: mongoose.Types.ObjectId(userId)})
	} catch (findError) {
		Logger.info(TAG, findError.toString())
		throw findError
	}
}

module.exports.addUser = async (newUserData) => {
	try {
		//let newUser = createNewUser(newUserData)
		let newUser = new User(newUserData)
		return await newUser.save()
	} catch (saveError) {
		Logger.info(TAG, saveError.toString())
		throw saveError
	}
}

module.exports.updateUser = async (targetUserId, updateData) => {
	try {
		return await User.findOneAndUpdate(
			{ _id: mongoose.Types.ObjectId(targetUserId) },
			{ $set: updateData },
			{ new: true } // Returns the updated object
		).exec()
	} catch (updateError) {
		Logger.info(TAG, updateError.toString())
		throw updateError
	}
}

module.exports.getAll = async () => {
	try {
		return await User.find().exec()
	} catch (getAllError) {
		Logger.info(TAG, getAllError.toString())
		throw getAllError
	}
}

module.exports.login = async (withUserData) => {
	return new Promise(async (resolve, reject) => {
		try {
			let options = {}
			if (withUserData.email) {
				options.email = withUserData.email
			} else if (withUserData.cpf) {
				options.cpf = withUserData.cpf
			} else {
				return reject(Errors.OPERATIONS.LOGIN)
			}
			let result = await User.findOne(options).exec()
			if (result && result.password) {
				if (await Hash.verifyPassword(withUserData.password, result.password)) {
					let token = await jwt.sign({
						email: result.email,
						cpf: result.cpf,
						name: result.name,
						userTypes: result.types,
						id: result._id,
					}, Constants.SESSION_SECRET)
					let response = {}
					response.token = 'JWT ' + token
					response.result = Successes.OPERATIONS.LOGIN
					return resolve(response)
				} else {
					Logger.info(TAG, 'Unsuccessful login attempt')
					let response = {}
					response.status = Errors.OPERATIONS.LOGIN
					return reject(response)
				}
			} else {
				Logger.info(TAG, 'Unsuccessful login attempt')
				let response = {}
				response.status = Errors.OPERATIONS.LOGIN
				return reject(response)
			}
		} catch (loginError) {
			return reject(loginError)
		}
	})
}