/**
 * Hella Backend API
 * routes/system/adminUserRoutes.js
 * Jean Oliveira 09/11/2017
 *
 * Admin User routes
 *
 */
'use strict'
let express = require('express'),
	router = express.Router(),
	HellaSchemas = require('../../utils/HellaSchemas'),
	Logger = require('../../utils/Logger'),
	Boom = require('boom'),
	System = require('../../models/classes/System'),
	Joi = require('joi'),
	dbUser = require('../../models/dbCrud/dbUser'),
	Error = require('../../utils/Errors')

const TAG = 'AdminUserRoutes'

const newUserSchema = HellaSchemas.ROUTE_SCHEMAS.NEW_USER_SCHEMA

/* GET existing users with action name. */
router.get('/:action', async (req, res, next) => {
	try {
		if (req.query && req.query.action) {
			Logger.info(TAG, 'GET Admin Users with action' + req.query.action)
			switch (req.query.action) {
				case 'list':
					res.status(200).json(await dbUser.getAll())
					break
				case 'read':
					if (req.query.userId) {
						res.status(200).json(await dbUser.findById(req.body.userId))
					} else {
						throw Error.OPERATIONS.INVALID_QUERY
					}
					break
				default:
					throw Error.OPERATIONS.INVALID_ACTION
			}
			res.status(200).json(await System.getContentForKey(req.query.propertyName))
		} else {
			throw Error.OPERATIONS.INVALID_ACTION
		}
	} catch (blockError) {
		if (blockError.status == 400) {
			res.status(400).json(Boom.badRequest('Bad Request', blockError))
		} else {
			res.status(501).json(Boom.notImplemented('I dont know what to do with this', getError))
		}
	}
})

/* POST data for a parameter name. */
router.post('/:action', async (req, res, next) => {
	if (req.query.action && req.query.action === 'create') {
		Logger.info(TAG, 'POST '+ req.params.propertyName)
		try {
			const result = await Joi.validate(req.body.user, newUserSchema)
			if (!result.error) {
				res.status(200).json(await dbUser.addUser(req.body.user))
			} else {
				res.status(400).json(Boom.badRequest('You must provide a valid object on the request BODY'), result.error)
			}
		} catch (getError) {
			Logger.error(TAG, getError.toString())
			res.status(501).json(Boom.notImplemented('I dont know what to do with this', getError))
		}
	} else {
		Logger.error(TAG, Error.PROPERTY.WRONG_MISSING)
		res.status(400).json(Boom.badRequest('You must provide a valid property name, mate!', Error.PROPERTY.WRONG_MISSING))
	}
})

module.exports = router