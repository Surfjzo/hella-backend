/**
 * Hella Backend API
 * routes/user/signUserRoutes.js
 * Jean Oliveira 25/11/2017
 *
 * Sign In and Sign Up User routes
 *
 */
'use strict'
let express = require('express'),
	router = express.Router(),
	Constants = require('../../utils/constants'),
	Logger = require('../../utils/Logger'),
	Boom = require('boom'),
	Successes = require('../../utils/Successes'),
	Errors = require('../../utils/Errors'),
	Joi = require('joi'),
	dbUser = require('../../models/dbCrud/dbUser'),
	HellaSchemas = require('../../utils/HellaSchemas')

const TAG = 'SignUserRoutes'

const newUserSchema = HellaSchemas.ROUTE_SCHEMAS.NEW_USER_SCHEMA,
	loginUserSchema = HellaSchemas.ROUTE_SCHEMAS.USER_LOGIN_SCHEMA

/* POST data for a parameter name. */
router.post('/:action', async (req, res, next) => {
	try {
		if (req.params.action) {
			Logger.info(TAG, 'POST Sign ' + req.params.action)
				switch (req.params.action) {
					case 'create':
					case 'up':
						return res.json(await createUser(req.body.user))
						break
					case 'login':
					case 'in':
						try {
							let loginAttempt = await loginUser(req.body.user)
							if (loginAttempt.result === Successes.OPERATIONS.LOGIN) {
								return res.json(loginAttempt)
							} else {
								throw Errors.OPERATIONS.LOGIN
							}
						} catch (loginError) {
							throw Errors.OPERATIONS.LOGIN
						}
						break
					default:
						throw Errors.OPERATIONS.INVALID_ACTION
				}
		} else {
			throw Errors.OPERATIONS.INVALID_ACTION
		}
	} catch (blockError) {
		if (blockError.status && blockError.content) {
			res.status(blockError.status).json(blockError.content)
		} else {
			res.status(501).json(Boom.notImplemented('I dont know what to do with this', blockError))
		}
	}
})

async function createUser(newUserData) {
	try {
		const result = await Joi.validate(newUserData, newUserSchema)
		if (!result.error) {
			return await dbUser.addUser(newUserData)
		} else {
			return Boom.badRequest('You must provide a valid object on the request BODY', result.error)
		}
	} catch (e) {
		throw e
	}
}

async function loginUser(userData) {
	try {
		const result = await Joi.validate(userData, loginUserSchema)
		if (!result.error) {
			return await dbUser.login(userData)
		} else {
			return Errors.OPERATIONS.LOGIN
		}
	} catch (e) {
		throw e
	}
}

module.exports = router